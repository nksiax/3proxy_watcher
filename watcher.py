import os
import subprocess
import time
import json
import logging
import logging.handlers
import re
import sys

try:
	import urllib.request as urllib
except ImportError:
	import urllib2 as urllib

try:
	from subprocess import DEVNULL
except ImportError:
	DEVNULL = open(os.devnull, 'w')


TOUCH = 1

RESTART = 2

DEFAULT_CONFIG = {
	'api_key': '',
	'server_name': '',
	'server_id': '',
	'config_dir': '/usr/local/3proxy',
	'3proxy_exec': '/bin/3proxy',
	'monitor': '3proxy.monitor',
	'config_name': 'proxyline',
	'kill_signal': '-9',
	'update_method': TOUCH,

	'update_interval': 10,
	'restart_interval': 0,
	'check_interval': 0,

	'log_filename': 'logs/watcher.log',
	'log_max_bytes': 1024 * 1024 * 10,
	'log_backup_count': 5,

	'request_timeout': 10,
	'request_retry': 1,
	'request_proxy': '',

	'dry_run': '--dry-run' in sys.argv,
	'host': 'panel.proxyline.net'
}


def get_config():
	config = DEFAULT_CONFIG.copy()

	with open('config.json', 'r') as f:
		config.update(json.loads(f.read()))

	if not config['server_id']:
		config['server_id'] = config['server_name']

	for param in ('3proxy_exec', 'monitor'):
		if config[param] and not config[param].startswith('/'):
			config[param] = os.path.join(config['config_dir'], config[param])

	return config


CONFIG = get_config()

HASH_URL = (
	'https://%(host)s/servers/%(server_id)s/config-hash/?api_key=%(api_key)s'
	% CONFIG
)

CONFIGS_URL = (
	'https://%(host)s/servers/%(server_id)s/configs/?api_key=%(api_key)s'
	% CONFIG
)

RE_CONFIG_NUM = re.compile(r'%s-(\d+).cfg' % CONFIG['config_name'])


def get_urlopener():
	if CONFIG['request_proxy']:
		handler = urllib.ProxyHandler({
			'http': CONFIG['request_proxy'],
			'https': CONFIG['request_proxy']
		})
		opener = urllib.build_opener(handler)
	else:
		opener = urllib.build_opener()

	opener.addheaders = [
		('Cache-Control', 'max-age=0'),
		('User-agent', 'Mozilla/5.0')
	]
	return opener


urlopener = get_urlopener()


def get_logger():
	if not CONFIG['log_filename'].startswith('/'):
		log_dir = CONFIG['log_filename'].split('/')[0]

		if log_dir:
			try:
				os.mkdir(log_dir)
			except OSError:
				pass

	console = logging.StreamHandler()
	file = logging.handlers.RotatingFileHandler(
		filename=CONFIG['log_filename'],
		maxBytes=CONFIG['log_max_bytes'],
		backupCount=CONFIG['log_backup_count']
	)
	formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s')
	console.setFormatter(formatter)
	file.setFormatter(formatter)

	logger = logging.getLogger('')
	logger.addHandler(console)
	logger.addHandler(file)
	logger.setLevel(logging.DEBUG)

	return logger


logger = get_logger()


def request(url):
	retry = (CONFIG['request_retry'] or 0) + 1

	while True:
		try:
			response = urlopener.open(url, timeout=CONFIG['request_timeout'])
		except:
			logger.exception('retry %s', retry)
			retry -= 1

			if retry == 0:
				raise
		else:
			break

	data = json.loads(response.read())
	logger.debug('response headers: \n%s', response.info())
	response.close()
	return data


def get_config_filename(num):
	return os.path.join(
		CONFIG['config_dir'], '%s-%s.cfg' % (CONFIG['config_name'], num)
	)


def get_config_list():
	lst = []

	for file in os.listdir(CONFIG['config_dir']):
		if file.startswith(CONFIG['config_name'] + '-') and file.endswith('.cfg'):
			lst.append(os.path.join(CONFIG['config_dir'], file))
	return lst


def get_config_num(config_filename):
	search = RE_CONFIG_NUM.search(config_filename)

	if search:
		return int(search.groups()[0])


def get_process_list():
	process = subprocess.Popen(
		['ps', '-eo', 'pid,args'], stdout=subprocess.PIPE, stderr=subprocess.PIPE
	)

	lst = []
	stdout, _ = process.communicate()

	for line in stdout.decode().splitlines():
		if CONFIG['3proxy_exec'] in line and CONFIG['config_name'] in line:
			lst.append(line.strip().split(' ', 1))

	return lst


def kill(pid, cmd=None):
	os.system('kill %s %s' % (CONFIG['kill_signal'], pid))
	logger.debug('killed pid:%s cmd:%s', pid, cmd)


def start_process(config_filename):
	args = [CONFIG['3proxy_exec'], config_filename]
	process = subprocess.Popen(
		args, shell=False, close_fds=True,
		stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL
	)
	logger.debug('popen 3proxy pid:%d command:%s', process.pid, args)
	return process


def normalize_config(config, config_filename):
	if not CONFIG['monitor']:
		try:
			index = config.index('monitor')
		except ValueError:
			pass
		else:
			if index and config[index - 1] != '#':
				config = '%s#%s' % (config[:index], config[index:])
		config = 'monitor %s\n%s' % (config_filename, config)
	return config


def update(hashes):
	if request(HASH_URL)['hash'] != hashes['main']:
		logger.debug('update')
		response = request(CONFIGS_URL)
		hashes['main'] = response['hash']
		config_count = len(response['configs'])
		logger.debug('config count: %d', config_count)
		process_list = get_process_list()

		for pid, cmd in process_list:
			num = get_config_num(cmd)

			if num and num > config_count:
				kill(pid, cmd)
				config_filename = get_config_filename(num)
				os.system('rm -f ' + config_filename)
				logger.debug('removed ' + config_filename)

				try:
					del hashes[num]
				except KeyError:
					pass

		for i, data in enumerate(response['configs'], start=1):
			if not CONFIG['monitor'] and hashes.get(i) == data['hash']:
				logger.debug('config %d skipped, hash: %s', i, data['hash'])
				continue

			started = False
			hashes[i] = data['hash']
			config_filename = get_config_filename(i)
			config = normalize_config(data['config'], config_filename)

			with open(config_filename, 'w') as f:
				f.write(config)

			if not any(config_filename in p[1] for p in process_list):
				start_process(config_filename)
				started = True

			if not CONFIG['monitor']:
				logger.debug('config %d updated', i)

				if CONFIG['update_method'] == RESTART and not started:
					restart(config_filename)

		if CONFIG['monitor']:
			if CONFIG['update_method'] == TOUCH:
				os.system('touch ' + CONFIG['monitor'])
				logger.debug('touch ' + CONFIG['monitor'])
			else:
				restart()
		logger.debug('update end')
	return hashes


def restart(config_filename=None):
	logger.debug('restart')

	for pid, cmd in get_process_list():
		if config_filename is None or config_filename in cmd:
			kill(pid, cmd)

	for config_name in get_config_list():
		if config_filename is None or config_filename in config_name:
			start_process(config_name)


def check():
	logger.debug('check')

	process_list = get_process_list()

	for config_filename in get_config_list():
		if not any(config_filename in p[1] for p in process_list):
			logger.debug('missing process: ' + config_filename)
			start_process(config_filename)


def main():
	if not CONFIG['api_key'] or not CONFIG['server_id']:
		logger.error('create config.json with api_key and server_id')
		return

	logger.debug('start')
	logger.debug('CONFIG:')
	logger.debug('\n'.join('%s: %s' % (k, v) for k, v in sorted(CONFIG.items())))

	hashes = {'main': None}
	restart_interval = time.time()
	check_interval = restart_interval
	update_interval = 0

	while True:
		if (CONFIG['restart_interval'] and
			time.time() - restart_interval > CONFIG['restart_interval']):

			try:
				restart()
			except:
				logger.exception('restart error')

			restart_interval = time.time()

		if (CONFIG['check_interval'] and
			time.time() - check_interval > CONFIG['check_interval']):

			try:
				check()
			except:
				logger.exception('check error')

			check_interval = time.time()

		if (CONFIG['update_interval'] and
			time.time() - update_interval > CONFIG['update_interval']):

			try:
				update(hashes)
			except:
				logger.exception('update error')

			update_interval = time.time()

		if CONFIG['dry_run']:
			break

		time.sleep(1)


if __name__ == '__main__':
	main()
